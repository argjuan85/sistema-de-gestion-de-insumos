<?php
class Stock extends CI_Controller {

 public function __construct()
        {
                parent::__construct();
                $this->load->library('grocery_CRUD');
                $this->load->library('session');
                $this->load->model('sedes_model');
                 $this->load->model('insumos_model');
                 $this->load->model('Stock_model');
                 $this->load->model('auth_model');
                $this->load->database();

                // Your own constructor code
        }



        public function view($page = 'home')
        {
          if ( ! file_exists(APPPATH.'/views/stock/'.$page.'.php'))
        {
                // Whoops, we don't have a page for that!
                show_404();
        }

        $data['title'] = ucfirst($page); // Capitalize the first letter

        $this->load->view('templates/header', $data);
        $this->load->view('sectores/'.$page, $data);
        $this->load->view('templates/footer', $data);
        }

//ajuste de stock (carga de vista)
function ajuste($id_insumo = ''){
	if	($this->general_model->validapermiso(131072, $this->session->userdata('permisosede'))) 
 		{
 			
	//valido sede
	 
	 $this->general_model->validasede($this->Stock_model->obtener_sede_stock($this->uri->segment(3)));
		
		
	//css y js de la vista a cargar
				
  	$data['js_files']=array('subvariable1'=> base_url().'assets/DataTables/media/js/jquery.js','subariable2'=>base_url().'assets/DataTables/media/js/jquery.jeditable.js','subariable3'=>base_url().'assets/DataTables/media/js/jquery.dataTables.js','subariable4'=>base_url().'assets/plugins/fancybox/jquery.fancybox.js?v=2.1.5');

$data['css_files']=array('subariable2'=>base_url().'assets/DataTables/media/css/demo_table.css','subariable3'=>base_url().'assets/plugins/fancybox/jquery.fancybox.css?v=2.1.5', 'subariable4'=>base_url().'assets/css/rangos_fechas.css');

$data['css_files2']=array('subariable2'=>base_url().'assets/css/reportes.css');

     	//$data['js_files']=array('subvariable1'=> base_url().'assets/datepicker/js/jquery-1.7.2.min.js','subariable2'=>base_url().'assets/datepicker/js/jquery-ui-1.8.20.custom.min.js' ,'subariable3'=>base_url().'assets/plugins/fancybox/jquery.fancybox.js?v=2.1.5');
		//$data['css_files']=array('subariable2'=>base_url().'assets/datepicker/css/ui-lightness/jquery-ui-1.8.20.custom.css', 'subariable3'=>base_url().'assets/css/rangos_fechas.css');		
		$data['motivos']  = $this->parametros_model->obtener_motivos();		
			  	  $data['error'] = "";
          $data['content_view']='stock/ajuste.php';
     	 //template diferente para vistas que no incluyen grocery 
    	   $data['menu_sede_oculto']="1";
    	$this->load->view('template2',$data);
			
	
		
}
}
// ajuste 
function ajuste2($id_insumo = ''){
	if	($this->general_model->validapermiso(131072, $this->session->userdata('permisosede'))) 
 		{
			//set validations
          $this->general_model->validasede($this->Stock_model->obtener_sede_stock($this->uri->segment(3)));
          //$this->form_validation->set_rules("from", "Desde", "trim|required");
          $this->form_validation->set_rules("cantidad", "cantidad", "trim|required|is_natural_no_zero|max_length[2]");
     	  $this->form_validation->set_rules("motivos", "motivos", "required|is_natural");// valido que seleccione una opcion con id positivo "seleccione" tiene asignado -1
          $this->form_validation->set_message('is_natural', 'Por favor seleccione una opción');

     	 if ($this->form_validation->run() == FALSE)// validacion campos
          {
          		//css y js de la vista a cargar
     	$data['js_files']=array('subvariable1'=> base_url().'assets/datepicker/js/jquery-1.7.2.min.js','subariable2'=>base_url().'assets/datepicker/js/jquery-ui-1.8.20.custom.min.js');
		$data['css_files']=array('subariable2'=>base_url().'assets/datepicker/css/ui-lightness/jquery-ui-1.8.20.custom.css', 'subariable3'=>base_url().'assets/css/rangos_fechas.css');		
          		$data['motivos']  = $this->parametros_model->obtener_motivos();		
			  	  
          	  	  $data['error'] = "";
              $data['content_view']='stock/ajuste.php';
     	 //template diferente para vistas que no incluyen grocery 
    	 $this->load->view('template2',$data);
     	}
     	else
     	{
				$cantidad =  $this->input->post('cantidad');
				$stock_actual = $this->stock_model->obtener_stock($id_insumo, $this->session->userdata('sede_filtro') );
			
			if ($stock_actual != $cantidad)
			{
				
						
			$insumo =  $id_insumo;
          
          	$motivo =  $this->input->post('motivos');
          	
			if ($stock_actual < $cantidad)
			{
				$diferencia =  $cantidad - $stock_actual;
				$signo = "+";
				$this->stock_model->actualiza_stock($insumo,$this->session->userdata('sede_filtro'),$cantidad);
			}
			else
			{
				$diferencia =  $stock_actual - $cantidad;
				$signo = "-";
				$this->stock_model->actualiza_stock($insumo,$this->session->userdata('sede_filtro'),$cantidad);
		
			
				  
			}
			     //log
			    $id = $this->stock_model->obtener_id_insumo($id_insumo, $this->session->userdata('sede_filtro'));
			    $registro = $this->insumos_model->obtener_codigo($id);
				 $this->general_model->registralog ( "M" , "Stock" , $id_insumo , $registro, "Ajuste de stock:(".$signo.$diferencia.") Motivo:".$this->parametros_model->obtener_parametro($motivo));
			     $this->session->set_flashdata('error', 'Se realizo el ajuste de stock correctamente.');
				  //redirect('stock/listar');
				  redirect(site_url("stock/listar"),"refresh");
			}
			else
			{
				  $this->session->set_flashdata('error', 'Debe ingresar una cantidad distinta a la actual.');
				       		$data['motivos']  = $this->parametros_model->obtener_motivos();		
			  	  //css y js de la vista a cargar
     	$data['js_files']=array('subvariable1'=> base_url().'assets/datepicker/js/jquery-1.7.2.min.js','subariable2'=>base_url().'assets/datepicker/js/jquery-ui-1.8.20.custom.min.js');
		$data['css_files']=array('subariable2'=>base_url().'assets/datepicker/css/ui-lightness/jquery-ui-1.8.20.custom.css', 'subariable3'=>base_url().'assets/css/rangos_fechas.css');		
          	  	  $data['error'] = "vacio";
              $data['content_view']='stock/ajuste.php';
     	 //template diferente para vistas que no incluyen grocery 
    	 $this->load->view('template2',$data);
				 
			}
	
		}
}
}


      function listar(){
     	$this->general_model->validasesion();
      	if  ($this->general_model->validapermiso(16, $this->session->userdata('permisosede')))
 		{
      	 	 //si hubo cambio de sede actualizo permisos y filtro sede
      	 	  if(isset($_POST['insumo']))
    	{
        $sede_consulta = $this->input->post('insumo');//sede nueva
        $this->auth_model->cambio_sede($sede_consulta);
    
    	}
    	else
    	{
				$sede_consulta= $this->general_model->ou_sede_id($this->session->userdata('sede'));
		}
      	
$this->grocery_crud->set_table('stock');
$this->grocery_crud->set_theme('Datatables');
$this->grocery_crud->set_model('Custom_grocery_crud_model');
if ($this->session->userdata('sede_filtro'))
        //$this->grocery_crud->where('id_sede',$this->session->userdata('sede_filtro'));
        $this->grocery_crud->basic_model->set_custom_query('select * from stock st inner join insumos i on i.id=st.id_insumo where st.id_sede = '.$this->session->userdata('sede_filtro')); //Query text here
	  	else
      	//$this->grocery_crud->where('id_sede',$sede_consulta);
		$this->grocery_crud->basic_model->set_custom_query('select * from stock st inner join insumos i on i.id=st.id_insumo where st.id_sede = '.$sede_consulta); //Query text here

$this->grocery_crud->set_language('spanish');
$this->grocery_crud->columns('codigo_insumo','stock_real','tipo_insumo','stock_minimo','cant_reorden');
$this->grocery_crud->add_fields('id_insumo','stock_real','stock_minimo','cant_reorden','habilitado','id_sede');
$this->grocery_crud->edit_fields('id_insumo','stock_real','stock_minimo','cant_reorden','habilitado');
//$this->grocery_crud->set_relation('id_insumo','insumos','codigo_insumo'); 
$this->grocery_crud->callback_column('tipo_insumo',array($this,'_callback_columna1'));

//$this->grocery_crud->set_relation('id_sede','sedes','nombre_sede'); 
$this->grocery_crud->unset_read_fields('id_sede','habilitado');	
$this->grocery_crud->display_as('id_insumo','Codigo de Insumo');
$this->grocery_crud->display_as('id_sede','Sede');
if ($this->grocery_crud->getState() == 'add') 
{
	 
     $this->grocery_crud->change_field_type('habilitado','invisible');
     $this->grocery_crud->change_field_type('id_sede','invisible');
}
if ($this->grocery_crud->getState() == 'edit') 
{

    if  ($this->general_model->validapermiso(131072, $this->session->userdata('permisosede'))) 
    {
	 $this->general_model->validasede($this->Stock_model->obtener_sede_stock($this->uri->segment(4)));
     $this->grocery_crud->change_field_type('habilitado','invisible');
     $this->grocery_crud->change_field_type('id_sede','invisible');
     $this->grocery_crud->field_type('stock_real','readonly'); 
     $this->grocery_crud->field_type('id_insumo','readonly'); 
   }
}
$this->grocery_crud->unset_add();
//$this->grocery_crud->unset_edit();
$this->grocery_crud->unset_read();
$this->grocery_crud->unset_delete();


//$this->grocery_crud->callback_column('habilitado',array($this,'_callback_columna'));
 //$this->grocery_crud->change_field_type('id_sede','invisible');
$this->grocery_crud->callback_before_insert(array($this,'before_insert1'));
$this->grocery_crud->add_action('Ajuste Stock', '', 'stock/ajuste','ui-icon-wrench');
//$this->grocery_crud->callback_column('Acciones',array($this,'callback_webpage_url2'));
//$this->grocery_crud->callback_edit_field('habilitado',array($this,'edit_field_callback_1'));
$output->css_files[] = base_url().'assets/css/general.css';
$output = $this->grocery_crud->render();
$output->content_view='crud_content_view';
$this->_example_output($output);

}
}

 function listar_minimos(){
     	$this->general_model->validasesion();
      	if  ($this->general_model->validapermiso(16, $this->session->userdata('permisosede')))
 		{
      	 	 //si hubo cambio de sede actualizo permisos y filtro sede
      	 	  if(isset($_POST['insumo']))
    	{
        $sede_consulta = $this->input->post('insumo');//sede nueva
        $this->auth_model->cambio_sede($sede_consulta);
    
    	}
    	else
    	{
				$sede_consulta= $this->general_model->ou_sede_id($this->session->userdata('sede'));
		}
      	
$this->grocery_crud->set_table('stock');
$this->grocery_crud->set_theme('Datatables');
if ($this->session->userdata('sede_filtro'))
        //$this->grocery_crud->where('id_sede',$this->session->userdata('sede_filtro'));
	  	$this->grocery_crud->where("stock_real < stock_minimo and id_sede='".$this->session->userdata('sede_filtro')."'",NULL);
	  	else
      	//$this->grocery_crud->where('id_sede',$sede_consulta);
		$this->grocery_crud->where("stock_real < stock_minimo and id_sede='".$sede_consulta."'",NULL);
     	// $where = "id_sede='".$this->session->userdata('sede_filtro')."' AND (tipo_pedido='E' OR tipo_pedido='N')";
      	
      	     
$this->grocery_crud->set_language('spanish');
$this->grocery_crud->columns('id_insumo','stock_real','stock_minimo','cant_reorden');
$this->grocery_crud->add_fields('id_insumo','stock_real','stock_minimo','cant_reorden','habilitado','id_sede');
$this->grocery_crud->edit_fields('id_insumo','stock_real','stock_minimo','cant_reorden','habilitado');
$this->grocery_crud->set_relation('id_insumo','insumos','codigo_insumo'); 

//$this->grocery_crud->set_relation('id_sede','sedes','nombre_sede'); 
$this->grocery_crud->unset_read_fields('id_sede','habilitado');	
$this->grocery_crud->display_as('id_insumo','Codigo de Insumo');
$this->grocery_crud->display_as('id_sede','Sede');
if ($this->grocery_crud->getState() == 'add') 
{
	
     $this->grocery_crud->change_field_type('habilitado','invisible');
     $this->grocery_crud->change_field_type('id_sede','invisible');
}
if ($this->grocery_crud->getState() == 'edit') 
{
 $this->general_model->validasede($this->Stock_model->obtener_sede_stock($this->uri->segment(4)));
     $this->grocery_crud->change_field_type('habilitado','invisible');
     $this->grocery_crud->change_field_type('id_sede','invisible');
     $this->grocery_crud->field_type('stock_real','readonly'); 
}
$this->grocery_crud->unset_add();
//$this->grocery_crud->unset_edit();
$this->grocery_crud->unset_read();
$this->grocery_crud->unset_delete();


//$this->grocery_crud->callback_column('habilitado',array($this,'_callback_columna'));
 //$this->grocery_crud->change_field_type('id_sede','invisible');
$this->grocery_crud->callback_before_insert(array($this,'before_insert1'));
$this->grocery_crud->add_action('Ajuste Stock', '', 'stock/ajuste','ui-icon-wrench');
//$this->grocery_crud->callback_column('Acciones',array($this,'callback_webpage_url2'));
//$this->grocery_crud->callback_edit_field('habilitado',array($this,'edit_field_callback_1'));
$output->css_files[] = base_url().'assets/css/general.css';
$output = $this->grocery_crud->render();
$output->content_view='crud_content_view';
$this->_example_output($output);

}
}
function _example_output($output = null){
// cargo template del sitio y envio la data a traves de output	
$this->load->view('template',$output);
} 




function before_insert1($post_array) {

   
$post_array['habilitado'] = "1";
$post_array['id_sede'] = $this->session->userdata('sede_filtro');

  return $post_array;
}

//reemplazo columnas en el listado gral 
public function _callback_columna($value, $row)
{
	if ($value == "1")
	{
return "Si";		
	}
  else
  {
  	return "No";
  }
}

//reemplazo columnas en el listado gral 
public function _callback_columna1($value, $row)
{

return $this->parametros_model->obtener_parametro($value);	

}
    
    
  


 
 
 
}
   