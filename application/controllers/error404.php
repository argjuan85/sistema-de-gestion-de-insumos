<?php
if (!defined('BASEPATH'))
   exit('No direct script access allowed');
class Error404 extends CI_Controller { 
  	public function index()
	{
		$principal =  site_url('/auth/principal');
		$this->session->set_flashdata('msg', '<div class="alert alert-danger text-center">Usted est&aacute; intentando acceder a una p&aacute;gina que no existe.<a href="'.$principal.'">Pagina Principal</a></div>');
		$data['content_view']='error404';
   
	 $this->load->view('template4',$data);
		
		
	}
}
?>