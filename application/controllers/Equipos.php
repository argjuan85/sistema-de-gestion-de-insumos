<?php
class Equipos extends CI_Controller {

 public function __construct()
        {
                parent::__construct();
                $this->load->library('grocery_CRUD');
                $this->load->library('session');
                $this->load->library('Audit');
                $this->load->model('sedes_model');
                $this->load->model('sectores_model');
                $this->load->model('stock_model');
                $this->load->model('Parametros_model');
                $this->load->model('entregas_model');
                 $this->load->model('equipos_model');
                $this->load->database();
				$this->load->helper('url'); 
				//$this->session->set_userdata('sede', "1");
                // Your own constructor code
        }



        public function view($page = 'home')
        {
          if ( ! file_exists(APPPATH.'/views/equipos/'.$page.'.php'))
        {
                // Whoops, we don't have a page for that!
                show_404();
        }

        $data['title'] = ucfirst($page); // Capitalize the first letter

        $this->load->view('templates/header', $data);
        $this->load->view('equipos/'.$page, $data);
        $this->load->view('templates/footer', $data);
        }

 // Ayudante de funciones de Filtrar DropDowns según el usuario logeado
 /*
function filter_dropdown($table, $id_table, $desc_table)
{

 $sectores = $this->sectores_model->obtener_sectores();	
  	$html = '<link type="text/css" rel="stylesheet" href="'.base_url().'/assets/grocery_crud/css/jquery_plugins/chosen/chosen.css" />';
	$html .= '<script src="'.base_url().'/assets/grocery_crud/js/jquery_plugins/jquery.chosen.min.js"></script>';
	$html .= '<script src="'.base_url().'/assets/grocery_crud/js/jquery_plugins/config/jquery.chosen.config.js"></script>';
 $html .= form_dropdown('id_sector', $sectores,'' , 'class="chosen-select" data-placeholder="Selecciona '.ucwords($table).'"');
//$html .= '<div class="form-button-box">';
$html .=	'&nbsp; <a href="'.site_url().'/sectores/listar/add" class="fancybox fancybox.iframe" data-fancybox-width="800" data-fancybox-height="370">Agregar Sector</a>';
 return $html; 
 // retorna el selectbox filtrado
 }
*/
      function listar(){
      	$this->general_model->validasesion();
      	if  ($this->general_model->validapermiso(64, $this->session->userdata('permisosede')))
 		{
      	  	 //si hubo cambio de sede actualizo permisos y filtro sede
      	 	  if(isset($_POST['insumo']))
    	{
        $sede_consulta = $this->input->post('insumo');//sede nueva
        $this->auth_model->cambio_sede($sede_consulta);
    
    	}
    	else
    	{
			$sede_consulta= $this->general_model->ou_sede_id($this->session->userdata('sede'));
		}
      	
      
$this->grocery_crud->set_table('equipos');
$this->grocery_crud->set_theme('Datatables');

/*$data['js_files']=array('subvariable1'=> base_url().'assets/DataTables/media/js/jquery.js','subariable2'=>base_url().'assets/DataTables/media/js/jquery.jeditable.js','subariable3'=>base_url().'assets/DataTables/media/js/jquery.dataTables.js','subariable4'=>base_url().'assets/plugins/fancybox/jquery.fancybox.js?v=2.1.5');
*//*
$this->grocery_crud->set_js("assets/js/fancy_test.js");
$this->grocery_crud->set_js("assets/DataTables/media/js/jquery.js");
$this->grocery_crud->set_js("assets/DataTables/media/js/jquery.jeditable.js");
$this->grocery_crud->set_js("assets/DataTables/media/js/jquery.dataTables.js");
$this->grocery_crud->set_js("assets/plugins/fancybox/jquery.fancybox.js?v=2.1.5");
/*$data['css_files']=array('subariable2'=>base_url().'assets/DataTables/media/css/demo_table.css','subariable3'=>base_url().'assets/plugins/fancybox/jquery.fancybox.css?v=2.1.5');*/
/*$this->grocery_crud->set_css("assets/DataTables/media/css/demo_table.css");
$this->grocery_crud->set_css("assets/plugins/fancybox/jquery.fancybox.css?v=2.1.5");*/

if ($this->session->userdata('sede_filtro'))
        $this->grocery_crud->where('Equipos.id_sede',$this->session->userdata('sede_filtro'));
	  	else
      	$this->grocery_crud->where('Equipos.id_sede',$sede_consulta);

$this->grocery_crud->set_language('spanish');
$this->grocery_crud->columns('codigo_equipo','id_modelo', 'serie', 'estado_equipo','observaciones','tipo_equipo','tipo_conexion','ip','id_sector','id_proveedor');
$this->grocery_crud->add_fields('codigo_equipo','id_modelo','serie', 'estado_equipo','observaciones','tipo_equipo','tipo_conexion','ip','id_sector','id_proveedor', 'id_sede', 'componentes');

 $this->grocery_crud->set_relation_n_n('componentes', 'componentes', 'insumos', 'id_equipo', 'id_insumo', 'codigo_insumo');

if ($this->grocery_crud->getState() == 'add') 
{
    $this->grocery_crud->change_field_type('id_sede','invisible');
  
    

}


//validacion
 //$this->grocery_crud->unique_fields('codigo_equipo','serie');
 
			$this->grocery_crud->set_rules('codigo_equipo', 'Equipo','trim|required|min_length[5]');
			$this->grocery_crud->set_rules('id_modelo', 'Modelo','trim|required');
			$this->grocery_crud->set_rules('estado_equipo', 'Estado','trim|required');
			$this->grocery_crud->set_rules('tipo_equipo', 'Tipo Equipo','trim|required');
			$this->grocery_crud->set_rules('id_proveedor', 'Proveedor','trim|required');
// obligo a cargar el sector independientemente del estado por que despues me puede traer problemas, y validar segun estado es un lio
			$this->grocery_crud->set_rules('id_sector', 'Sector','trim|required');

$this->grocery_crud->unset_read_fields('id_sede','habilitado');	
$this->grocery_crud->change_field_type('id_sede','invisible');
if ($this->grocery_crud->getState() == 'edit') 
{
 $this->general_model->validasede($this->equipos_model->obtener_sede_equipo($this->uri->segment(4)));
   
}

$this->grocery_crud->display_as('id_proveedor','Proveedor');
$this->grocery_crud->display_as('id_sector','Sector');
$this->grocery_crud->display_as('id_sede','Sede');
$this->grocery_crud->display_as('id_modelo','Modelo');

$this->grocery_crud->set_relation('id_proveedor','proveedores','nombre_proveedor'); 
$this->grocery_crud->set_relation('id_sector','sectores','nombre_sector', 'id_sede= "'.$this->session->userdata('sede_filtro').'"'); 
$this->grocery_crud->set_relation('estado_equipo','parametros','valor', 'nombre_parametro="estado equipo" and habilitado="1"'); 
//idem entregas, no hay crud para agregar estados libremente, se debe evaluar ya que de este estado dependen ciertos eventos y condiciones en otra parte del sistema. deben agragarse a mano desde la tabla parametros.

/**
* 
* @var /deberia obtener el id  del nombre del parametro para "tipo_modelo"
* 
*/
//aca habria que hacer una relacion una vez seleccionado el combo el tipo de equipo para que traiga el tipo de modelo
//de momento se dejan todos los modelos cargados
//$id_param = $this->Parametros_model->obtener_id_parametro("tipo equipo","impresora");
$this->grocery_crud->set_relation('id_modelo','modelos','nombre_modelo', 'habilitado="1"'); 

$this->grocery_crud->set_relation('tipo_equipo','parametros','valor', 'nombre_parametro="tipo equipo" and habilitado="1"'); 
$this->grocery_crud->set_relation('tipo_conexion','parametros','valor', 'nombre_parametro="tipo conexion" and habilitado="1"'); 


$this->grocery_crud->callback_before_insert(array($this,'before_insert1'));
$this->grocery_crud->callback_after_insert(array($this,'after_insert1'));
$this->grocery_crud->callback_before_update(array($this,'before_update1'));
$this->grocery_crud->callback_after_update(array($this, 'after_update1'));
$this->grocery_crud->callback_before_delete(array($this,'before_delete'));
//$this->grocery_crud->callback_field ( 'id_sector', array ( $this, '_callback_field_sector' ) );
$this->grocery_crud->set_lang_string('delete_error_message', 'Imposible eliminar el equipo, el mismo posee registros asociados');
$this->grocery_crud->set_lang_string('delete_success_message', 'El equipo se ha eliminado correctamente');
$output = $this->grocery_crud->render();
$output->content_view='crud_content_view';
$this->_example_output($output);

}
}

      
function _example_output($output = null){
// cargo template del sitio y envio la data a traves de output	
$this->load->view('template',$output);
} 

function before_insert1($post_array) {
$post_array['id_sede'] = $this->session->userdata('sede_filtro');

//$this->audit->register();
if ($this->parametros_model->verificar_registro_repetido($post_array['codigo_equipo'], "codigo_equipo" , "equipos", "id"))

{  
return $post_array;
}
else
{
	return false;
}
  
}

function before_update1($post_array, $primary_key) {

  if ($this->parametros_model->verificar_registro_repetido($post_array['codigo_equipo'], "codigo_equipo" , "equipos", "id", $primary_key))

{  
return $post_array;
}
else
{
	return false;
}

}


function after_insert1($post_array, $primary_key) {
$registro = $this->equipos_model->obtener_codigo($primary_key);
$this->general_model->registralog ( "I" , "Equipos" , $primary_key , $registro );
}


function after_update1($post_array, $primary_key) {
$registro = $this->equipos_model->obtener_codigo($primary_key);
$this->general_model->registralog ( "M" , "Equipos" , $primary_key , $registro );
}
public function before_delete($primary_key)
{
    //funcion para chequear si el equipo debe ser borrado o no (enganches en las tablas.) (entregas)
    //$band= false;
  	$band= $this->entregas_model->verificar_entregas($primary_key);
 
    if($band)
        {return false;}
	else
    	{
    		$registro = $this->equipos_model->obtener_codigo($primary_key);
			$this->general_model->registralog ( "B" , "Equipos" , $primary_key , $registro );
    		return true;}
}

// Filtrar DropDowns según el usuario logeado
 function _callback_field_sector($value = '', $primary_key = null)
 {   
 return $this->filter_dropdown('sectores','id','nombre_sector');
 }

/*
//reemplazo por combo en add
function callback_add1($value, $primary_key)
{  
   return '<select id="tipo_conexion" name="tipo_conexion">
        <option value="red">Red</option>
        <option value="local">Local</option>
         </select>';

}

function callback_add2($value, $primary_key)
{  
   return '<select id="estado_equipo" name="estado_equipo">
        <option value="">Seleccione...</option>
        <option value="Operativa">Operativa</option>
        <option value="Backup">Backup</option>
        <option value="Ok">Ok</option>
        <option value="Rota">Rota</option>
      </select>';

}

/* revisar estaria bueno que el combo se arme tomando de parametros pensando a futuro  */
/*
function callback_add3($value, $primary_key)
{  
   return '<select id="tipo_equipo" name="tipo_equipo">
        <option value="">Seleccione...</option>
        <option value="Operativa">Operativa</option>
        <option value="Backup">Backup</option>
        <option value="Ok">Ok</option>
        <option value="Rota">Rota</option>
      </select>';

}


function callback_edit1($value, $primary_key)
{  
   return '<select id="estado_equipo" name="estado_equipo">
        <option value="">Seleccione...</option>
        <option value="Operativa">Operativa</option>
        <option value="Backup">Backup</option>
        <option value="Ok">Ok</option>
        <option value="Rota">Rota</option>
      </select>';

}
*/

}
?>