<?php
class Parametros extends CI_Controller {

 public function __construct()
        {
                parent::__construct();
                $this->load->library('grocery_CRUD');
                $this->load->model('Parametros_model');
                $this->load->library('session');
                 $this->load->model('sedes_model');
                 $this->load->model('stock_model');
                $this->load->database();

                // Your own constructor code
        }



        public function view($page = 'home')
        {
          if ( ! file_exists(APPPATH.'/views/parametros/'.$page.'.php'))
        {
                // Whoops, we don't have a page for that!
                show_404();
        }

        $data['title'] = ucfirst($page); // Capitalize the first letter

        $this->load->view('templates/header', $data);
        $this->load->view('parametros/'.$page, $data);
        $this->load->view('templates/footer', $data);
        }

  //motivos ajuste de stock  para cargar desde la vista de ajuste
      function motivos(){
      			if	($this->general_model->validapermiso(131072, $this->session->userdata('permisosede'))) 
 		{
      	  	 	  if(isset($_POST['insumo']))
    	{

        $sede_consulta = $this->input->post('insumo');
        $this->session->set_userdata('sede_filtro', $sede_consulta );

    	}
    	else
    	{
			$sede_consulta= $this->general_model->ou_sede_id($this->session->userdata('sede'));
		}
		
$this->grocery_crud->set_table('parametros');
$this->grocery_crud->set_theme('Datatables');
$this->grocery_crud->columns('valor', 'habilitado');
$this->grocery_crud->add_fields('nombre_parametro','id_sede','valor','tipo_parametro', 'habilitado');
$this->grocery_crud->edit_fields('valor', 'habilitado');
$this->grocery_crud->unset_read();
$this->grocery_crud->unset_back_to_list();
$this->grocery_crud->unset_delete();//no se pueden eliminar ya que el unico "enganche" que habra con estos registros es en la tabla log campo observaciones lo cual se hace dificil de trackear. Solo se deshabilitaran para que no figuren en el combo
$this->grocery_crud->where('nombre_parametro = "motivo_ajuste"',NULL);
$this->grocery_crud->change_field_type('id_sede','invisible');
$this->grocery_crud->change_field_type('tipo_parametro','invisible');
$this->grocery_crud->change_field_type('nombre_parametro','invisible');

 //trae inconvenientes en el edit
// $this->grocery_crud->unique_fields('valor');

$this->grocery_crud->set_rules('valor', 'Motivo ajuste','trim|required|min_length[2]');

if ($this->grocery_crud->getState() == 'add') 
{
$this->grocery_crud->change_field_type('habilitado','invisible'); 
}

$this->grocery_crud->display_as('valor','Motivo');
$this->grocery_crud->unset_read_fields('id_sede','tipo');	
$this->grocery_crud->set_language('spanish');
$this->grocery_crud->callback_before_insert(array($this,'before_insert_p'));

$output = $this->grocery_crud->render();
//$output->content_view='crud_content_view';
$this->_example_output2($output);

}}
//motivos ajuste de stock  para cargar desde la vista de menu
    function motivos2(){
      		if	($this->general_model->validapermiso(131072, $this->session->userdata('permisosede'))) 
 		{
      	  	 	  if(isset($_POST['insumo']))
    	{

        $sede_consulta = $this->input->post('insumo');
        $this->session->set_userdata('sede_filtro', $sede_consulta );

    	}
    	else
    	{
			$sede_consulta= $this->general_model->ou_sede_id($this->session->userdata('sede'));
		}
		
$this->grocery_crud->set_table('parametros');
$this->grocery_crud->set_theme('Datatables');
$this->grocery_crud->columns('valor', 'habilitado');
$this->grocery_crud->add_fields('nombre_parametro','id_sede','valor','tipo_parametro', 'habilitado');
$this->grocery_crud->edit_fields('valor', 'habilitado');
$this->grocery_crud->unset_read();
//$this->grocery_crud->unset_back_to_list();
$this->grocery_crud->unset_delete();//no se pueden eliminar ya que el unico "enganche" que habra con estos registros es en la tabla log campo observaciones lo cual se hace dificil de trackear. Solo se deshabilitaran para que no figuren en el combo
$this->grocery_crud->where('nombre_parametro = "motivo_ajuste"',NULL);
$this->grocery_crud->change_field_type('id_sede','invisible');
$this->grocery_crud->change_field_type('tipo_parametro','invisible');
$this->grocery_crud->change_field_type('nombre_parametro','invisible');

 //trae inconvenientes en el edit
// $this->grocery_crud->unique_fields('valor');

$this->grocery_crud->set_rules('valor', 'Motivo ajuste','trim|required|min_length[2]');

if ($this->grocery_crud->getState() == 'add') 
{
$this->grocery_crud->change_field_type('habilitado','invisible'); 
}

$this->grocery_crud->display_as('valor','Motivo');
$this->grocery_crud->unset_read_fields('id_sede','tipo');	
$this->grocery_crud->set_language('spanish');
$this->grocery_crud->callback_before_insert(array($this,'before_insert_p'));

$output = $this->grocery_crud->render();
$output->content_view='crud_content_view';
$this->_example_output($output);
}
}
  //crud tipo de equipos
      function tipos_equipo(){
      			if	($this->general_model->validapermiso(131072, $this->session->userdata('permisosede'))) 
 		{
      	  	 	  if(isset($_POST['insumo']))
    	{

        $sede_consulta = $this->input->post('insumo');
        $this->session->set_userdata('sede_filtro', $sede_consulta );

    	}
    	else
    	{
			$sede_consulta= $this->general_model->ou_sede_id($this->session->userdata('sede'));
		}
		
$this->grocery_crud->set_table('parametros');
$this->grocery_crud->set_theme('Datatables');
$this->grocery_crud->columns('valor', 'habilitado');
$this->grocery_crud->add_fields('nombre_parametro','id_sede','valor','tipo_parametro', 'habilitado');
$this->grocery_crud->edit_fields('nombre_parametro','id_sede','valor','tipo_parametro', 'habilitado');
//$this->grocery_crud->edit_fields('valor', 'habilitado');
$this->grocery_crud->unset_read();
$this->grocery_crud->unset_delete();//no se pueden eliminar ya que el unico "enganche" que habra con estos registros es en la tabla log campo observaciones lo cual se hace dificil de trackear. Solo se deshabilitaran para que no figuren en el combo
$this->grocery_crud->where('nombre_parametro = "tipo equipo"',NULL);
$this->grocery_crud->change_field_type('id_sede','invisible');
$this->grocery_crud->change_field_type('tipo_parametro','invisible');
$this->grocery_crud->change_field_type('nombre_parametro','invisible');

//trae inconvenientes en el edit
// $this->grocery_crud->unique_fields('valor');


if ($this->grocery_crud->getState() == 'add') 
{

$this->grocery_crud->change_field_type('habilitado','invisible'); 
}

if ($this->grocery_crud->getState() == 'edit') 
{
   
     $this->grocery_crud->field_type('valor','readonly'); 
}
$this->grocery_crud->display_as('valor','Tipo de Equipo');
$this->grocery_crud->unset_read_fields('id_sede','tipo');	
$this->grocery_crud->set_language('spanish');
$this->grocery_crud->callback_before_insert(array($this,'before_insert_t'));



$output = $this->grocery_crud->render();
$output->content_view='crud_content_view';
$this->_example_output($output);

}}

  // crud tipo de conexion
      function tipos_conexion(){
      				if	($this->general_model->validapermiso(131072, $this->session->userdata('permisosede'))) 
 		{
      	  	 	  if(isset($_POST['insumo']))
    	{

        $sede_consulta = $this->input->post('insumo');
        $this->session->set_userdata('sede_filtro', $sede_consulta );

    	}
    	else
    	{
			$sede_consulta= $this->general_model->ou_sede_id($this->session->userdata('sede'));
		}
		
$this->grocery_crud->set_table('parametros');
$this->grocery_crud->set_theme('Datatables');
$this->grocery_crud->columns('valor', 'habilitado');
$this->grocery_crud->add_fields('nombre_parametro','id_sede','valor','tipo_parametro', 'habilitado');
$this->grocery_crud->edit_fields('valor', 'habilitado');
$this->grocery_crud->unset_read();
$this->grocery_crud->unset_delete();
$this->grocery_crud->where('nombre_parametro = "tipo conexion"',NULL);
$this->grocery_crud->change_field_type('id_sede','invisible');
$this->grocery_crud->change_field_type('tipo_parametro','invisible');
$this->grocery_crud->change_field_type('nombre_parametro','invisible');

 //trae inconvenientes en el edit
// $this->grocery_crud->unique_fields('valor');
$this->grocery_crud->set_rules('valor', 'Tipo Conexion','trim|min_length[2]');

if ($this->grocery_crud->getState() == 'add') 
{


$this->grocery_crud->change_field_type('habilitado','invisible'); 
}
if ($this->grocery_crud->getState() == 'edit') 
{
   
     $this->grocery_crud->field_type('valor','readonly'); 
}
$this->grocery_crud->display_as('valor','Tipo de Conexion');
$this->grocery_crud->unset_read_fields('id_sede','tipo');	
$this->grocery_crud->set_language('spanish');
$this->grocery_crud->callback_before_insert(array($this,'before_insert_t2'));
$output = $this->grocery_crud->render();
$output->content_view='crud_content_view';
$this->_example_output($output);

}}

  // crud tipo de insumo
      function tipos_insumo(){
      				if	($this->general_model->validapermiso(131072, $this->session->userdata('permisosede'))) 
 		{
      	  	 	  if(isset($_POST['insumo']))
    	{

        $sede_consulta = $this->input->post('insumo');
        $this->session->set_userdata('sede_filtro', $sede_consulta );

    	}
    	else
    	{
			$sede_consulta= $this->general_model->ou_sede_id($this->session->userdata('sede'));
		}
		
$this->grocery_crud->set_table('parametros');
$this->grocery_crud->set_theme('Datatables');
$this->grocery_crud->columns('valor', 'habilitado');
$this->grocery_crud->add_fields('nombre_parametro','id_sede','valor','tipo_parametro', 'habilitado');
$this->grocery_crud->edit_fields('valor', 'habilitado');
$this->grocery_crud->unset_read();
$this->grocery_crud->unset_delete();
$this->grocery_crud->where('nombre_parametro = "tipo insumo"',NULL);
$this->grocery_crud->change_field_type('id_sede','invisible');
$this->grocery_crud->change_field_type('tipo_parametro','invisible');
$this->grocery_crud->change_field_type('nombre_parametro','invisible');
$this->grocery_crud->display_as('valor','Tipo de Insumo');
//trae inconvenientes en el edit
// $this->grocery_crud->unique_fields('valor');

//$this->grocery_crud->set_rules('valor', 'Tipo de Insumo','trim|required|min_length[2]');

if ($this->grocery_crud->getState() == 'add') 
{
	//$this->grocery_crud->set_rules('nombre_parametro', 'Tipo de Insumo','trim|required|min_length[2]');
	
$this->grocery_crud->change_field_type('habilitado','invisible'); 
}
if ($this->grocery_crud->getState() == 'edit') 
{
   
     $this->grocery_crud->field_type('valor','readonly'); 
}
$this->grocery_crud->display_as('valor','Tipo de Insumo');
$this->grocery_crud->unset_read_fields('id_sede','tipo');	
$this->grocery_crud->set_language('spanish');
$this->grocery_crud->callback_before_insert(array($this,'before_insert_t3'));

$output = $this->grocery_crud->render();
$output->content_view='crud_content_view';
$this->_example_output($output);

}}

 // crud marca
      function marcas(){
      				if	($this->general_model->validapermiso(131072, $this->session->userdata('permisosede'))) 
 		{
      	
      	  	 	  if(isset($_POST['insumo']))
    	{

        $sede_consulta = $this->input->post('insumo');
        $this->session->set_userdata('sede_filtro', $sede_consulta );

    	}
    	else
    	{
			$sede_consulta= $this->general_model->ou_sede_id($this->session->userdata('sede'));
		}
		
$this->grocery_crud->set_table('parametros');
$this->grocery_crud->set_theme('Datatables');
$this->grocery_crud->columns('valor', 'habilitado');
$this->grocery_crud->add_fields('nombre_parametro','id_sede','valor','tipo_parametro', 'habilitado');
$this->grocery_crud->edit_fields('valor', 'habilitado');
$this->grocery_crud->unset_read();
$this->grocery_crud->unset_delete();
$this->grocery_crud->where('nombre_parametro = "marca"',NULL);
$this->grocery_crud->change_field_type('id_sede','invisible');
$this->grocery_crud->change_field_type('tipo_parametro','invisible');
$this->grocery_crud->change_field_type('nombre_parametro','invisible');

//trae inconvenientes en el edit
// $this->grocery_crud->unique_fields('valor');

//$this->grocery_crud->set_rules('valor', 'Marca','trim|required|min_length[2]');


if ($this->grocery_crud->getState() == 'add') 
{
	
$this->grocery_crud->change_field_type('habilitado','invisible'); 
}
if ($this->grocery_crud->getState() == 'edit') 
{
   
     $this->grocery_crud->field_type('valor','readonly'); 
}
$this->grocery_crud->display_as('valor','Marca');
$this->grocery_crud->unset_read_fields('id_sede','tipo');	
$this->grocery_crud->set_language('spanish');
$this->grocery_crud->callback_before_insert(array($this,'before_insert_t4'));

$output = $this->grocery_crud->render();
$output->content_view='crud_content_view';
$this->_example_output($output);

}}

 // crud tipo recepcion
      function tipo_recepcion(){
      				if	($this->general_model->validapermiso(131072, $this->session->userdata('permisosede'))) 
 		{
      	  	 	  if(isset($_POST['insumo']))
    	{

        $sede_consulta = $this->input->post('insumo');
        $this->session->set_userdata('sede_filtro', $sede_consulta );

    	}
    	else
    	{
			$sede_consulta= $this->general_model->ou_sede_id($this->session->userdata('sede'));
		}
		
$this->grocery_crud->set_table('parametros');
$this->grocery_crud->set_theme('Datatables');
$this->grocery_crud->columns('valor', 'habilitado');
$this->grocery_crud->add_fields('nombre_parametro','id_sede','valor','tipo_parametro', 'habilitado');
$this->grocery_crud->edit_fields('valor', 'habilitado');
$this->grocery_crud->unset_read();
$this->grocery_crud->unset_delete();
$this->grocery_crud->where('nombre_parametro = "tipo_recepcion"',NULL);
$this->grocery_crud->change_field_type('id_sede','invisible');
$this->grocery_crud->change_field_type('tipo_parametro','invisible');
$this->grocery_crud->change_field_type('nombre_parametro','invisible');


//trae inconvenientes en el edit
// $this->grocery_crud->unique_fields('valor');

	//$this->grocery_crud->set_rules('valor', 'Tipo  Recepcion','trim|required|min_length[2]');


if ($this->grocery_crud->getState() == 'add') 
{

$this->grocery_crud->change_field_type('habilitado','invisible'); 
}
if ($this->grocery_crud->getState() == 'edit') 
{
   
     $this->grocery_crud->field_type('valor','readonly'); 
}
$this->grocery_crud->display_as('valor','Tipo Recepción');
$this->grocery_crud->unset_read_fields('id_sede','tipo');	
$this->grocery_crud->set_language('spanish');
$this->grocery_crud->callback_before_insert(array($this,'before_insert_t5'));

$output = $this->grocery_crud->render();
$output->content_view='crud_content_view';
$this->_example_output($output);
}
}

 // crud tipo recepcion
      function mail_sede(){
      	
      	  	 	  if(isset($_POST['insumo']))
    	{

        $sede_consulta = $this->input->post('insumo');
        $this->session->set_userdata('sede_filtro', $sede_consulta );

    	}
    	else
    	{
			$sede_consulta= $this->general_model->ou_sede_id($this->session->userdata('sede'));
		}
		
$this->grocery_crud->set_table('parametros');
$this->grocery_crud->set_theme('Datatables');
$this->grocery_crud->columns('nombre_parametro','valor', 'habilitado');
$this->grocery_crud->add_fields('nombre_parametro','id_sede','valor','tipo_parametro', 'habilitado');
$this->grocery_crud->edit_fields('valor', 'habilitado');
$this->grocery_crud->unset_read();
$this->grocery_crud->unset_add();
$this->grocery_crud->unset_delete();
$this->grocery_crud->where('nombre_parametro LIKE "%mail_%" and id_sede='.$this->session->userdata('sede_filtro'),NULL);
$this->grocery_crud->change_field_type('id_sede','invisible');

 //$this->grocery_crud->unique_fields('valor');
$this->grocery_crud->set_rules('valor', 'Tipo  Recepcion','trim|required|min_length[2]');

if ($this->grocery_crud->getState() == 'add') 
{
$this->grocery_crud->change_field_type('habilitado','invisible'); 
}

$this->grocery_crud->display_as('valor','valor');
$this->grocery_crud->display_as('tipo_parametro','Tipo Mail');
$this->grocery_crud->unset_read_fields('id_sede','tipo');	
$this->grocery_crud->set_language('spanish');
$this->grocery_crud->callback_before_insert(array($this,'before_insert_t5'));

$output = $this->grocery_crud->render();
$output->content_view='crud_content_view';
$this->_example_output($output);

}




   // revisar este crud, ver como va quedar
      function listar(){
      	
      	  	 	  if(isset($_POST['insumo']))
    	{

        $sede_consulta = $this->input->post('insumo');
        $this->session->set_userdata('sede_filtro', $sede_consulta );

    	}
    	else
    	{
			$sede_consulta= $this->general_model->ou_sede_id($this->session->userdata('sede'));
		}
		
$this->grocery_crud->set_table('parametros');
$this->grocery_crud->set_theme('Datatables');
$this->grocery_crud->columns('nombre_parametro','valor');
$this->grocery_crud->add_fields('nombre_parametro','id_sede','valor','tipo_parametro');
$this->grocery_crud->edit_fields('nombre_parametro','valor');
$this->grocery_crud->unset_delete();
$this->grocery_crud->change_field_type('id_sede','invisible');
$this->grocery_crud->change_field_type('tipo_parametro','invisible');
$this->grocery_crud->unset_read_fields('id_sede','tipo');	
$this->grocery_crud->set_language('spanish');
$this->grocery_crud->callback_before_update(array($this,'before_update_t1'));

if ($this->grocery_crud->getState() == 'add') 
{
$this->grocery_crud->set_relation('nombre_parametro','parametros','valor', 'nombre_parametro="tipo parametro"'); 
}

$output = $this->grocery_crud->render();
$output->content_view='crud_content_view';
$this->_example_output($output);

}

function _example_output($output = null){
// cargo template del sitio y envio la data a traves de output	
$this->load->view('template',$output);
} 


function _example_output2($output = null){
$this->load->view('example',$output);
} 
/*
function _example_output($output = null){
// cargo template del sitio y envio la data a traves de output	
$this->load->view('template',$output);
} */



public function _callback_columna($value, $row)
{
	if ($value == "1")
	{
return "Si";		
	}
  else
  {
  	return "No";
  }
}


function before_insert1($post_array) {

$id_param = $this->Parametros_model->obtener_parametro($post_array['nombre_parametro']);
$post_array['id_sede'] = $this->session->userdata('sede');;
$post_array['tipo_parametro'] = "u";
$post_array['nombre_parametro'] = $id_param;
$post_array['tipo_parametro'] = "1";

//$post_array['nombre_parametro'] = ;

//echo $aux;
  return $post_array;
} 

function before_insert_p($post_array) {


$post_array['id_sede'] = "0";//es para todas las sedes
$post_array['tipo_parametro'] = "u";
$post_array['nombre_parametro'] = "motivo_ajuste";
$post_array['habilitado'] = "1";

  if ($this->parametros_model->verificar_param_repetido($post_array['nombre_parametro'] , $post_array['valor']))

{  
return $post_array;}
else
{
	return false;
}
} 

function before_insert_t($post_array) {


$post_array['id_sede'] = "0";//es para todas las sedes
$post_array['tipo_parametro'] = "u";
$post_array['nombre_parametro'] = "tipo equipo";
$post_array['habilitado'] = "1";

 if (!$this->parametros_model->verificar_param_vacio($post_array['valor']))

{  
 if ($this->parametros_model->verificar_param_repetido($post_array['nombre_parametro'] , $post_array['valor']))

{  
return $post_array;}
else
{
	return false;
}
}
else
{
	return false;
}
} 

function before_insert_t2($post_array) {


$post_array['id_sede'] = "0";//es para todas las sedes
$post_array['tipo_parametro'] = "u";
$post_array['nombre_parametro'] = "tipo conexion";
$post_array['habilitado'] = "1";
 if (!$this->parametros_model->verificar_param_vacio($post_array['valor']))

{  
 if ($this->parametros_model->verificar_param_repetido($post_array['nombre_parametro'] , $post_array['valor']))

{  
return $post_array;}
else
{
	return false;
}
}
else
{
	return false;
}

}



function before_insert_t3($post_array) {


$post_array['id_sede'] = "0";//es para todas las sedes
$post_array['tipo_parametro'] = "u";
$post_array['nombre_parametro'] = "tipo insumo";
$post_array['habilitado'] = "1";

 if (!$this->parametros_model->verificar_param_vacio($post_array['valor']))

{  
 if ($this->parametros_model->verificar_param_repetido($post_array['nombre_parametro'] , $post_array['valor']))

{  
return $post_array;}
else
{
	return false;
}
}
else
{
	return false;
}

}

function before_insert_t4($post_array) {


$post_array['id_sede'] = "0";//es para todas las sedes
$post_array['tipo_parametro'] = "u";
$post_array['nombre_parametro'] = "marca";
$post_array['habilitado'] = "1";

 if (!$this->parametros_model->verificar_param_vacio($post_array['valor']))

{  
 if ($this->parametros_model->verificar_param_repetido($post_array['nombre_parametro'] , $post_array['valor']))

{  
return $post_array;}
else
{
	return false;
}
}
else
{
	return false;
}

}

function before_insert_t5($post_array) {


$post_array['id_sede'] = "0";//es para todas las sedes
$post_array['tipo_parametro'] = "u";
$post_array['nombre_parametro'] = "tipo_recepcion";
$post_array['habilitado'] = "1";
 
  if (!$this->parametros_model->verificar_param_vacio($post_array['valor']))

{  
 if ($this->parametros_model->verificar_param_repetido($post_array['nombre_parametro'] , $post_array['valor']))

{  
return $post_array;}
else
{
	return false;
}
}
else
{
	return false;
}
 

}

function before_update_t1($post_array) {


$post_array['id_sede'] = $this->session->userdata('sede_filtro');//es para todas las sedes
//$post_array['tipo_parametro'] = "s";
//$post_array['nombre_parametro'] = "tipo_recepcion";
//$post_array['habilitado'] = "1";
if ($post_array['nombre_parametro'] == "mail_pedido_externo")
{
	$post_array['valor'] = "Se enviará a la casilla cargada en cada proveedor";
}
  return $post_array;
}

}
