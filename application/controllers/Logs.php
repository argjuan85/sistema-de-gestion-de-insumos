<?php
class Logs extends CI_Controller {

 public function __construct()
        {
                parent::__construct();
                $this->load->library('grocery_CRUD');
                $this->load->library('session');
                $this->load->model('sedes_model');
                $this->load->model('stock_model');
                $this->load->model('equipos_model');
                $this->load->model('sectores_model');
                $this->load->model('general_model');
                $this->load->model('entregas_model');
                $this->load->database();
                

                // Your own constructor code
        }



        public function view($page = 'home')
        {
          if ( ! file_exists(APPPATH.'/views/logs/'.$page.'.php'))
        {
                // Whoops, we don't have a page for that!
                show_404();
        }

        $data['title'] = ucfirst($page); // Capitalize the first letter

        $this->load->view('templates/header', $data);
        $this->load->view('sectores/'.$page, $data);
        $this->load->view('templates/footer', $data);
        }

      function listar($error = ''){
      	
      		$this->general_model->validasesion();
      	if	($this->general_model->validapermiso(131072, $this->session->userdata('permisosede')))
 		{
      	
      	
       	 //si hubo cambio de sede actualizo permisos y filtro sede
      	 	  if(isset($_POST['insumo']))
    	{
        $sede_consulta = $this->input->post('insumo');//sede nueva
        $this->auth_model->cambio_sede($sede_consulta);
    
    	}
    	else
    	{
			$sede_consulta= $this->general_model->ou_sede_id($this->session->userdata('sede'));
		}
      	
      	
      	
$this->grocery_crud->set_table('logs');
$this->grocery_crud->where('logs.id_sede',$this->session->userdata('sede_filtro'));
$this->grocery_crud->set_theme('Datatables');

 	//set validations
         
          $this->form_validation->set_rules("from", "Desde", "trim|required");
          $this->form_validation->set_rules("to", "Hasta", "trim|required");
     	


     	 if ($this->form_validation->run() == FALSE)// validacion campos
          {
     		$data['titulo_reporte'] = "Informe de logs";
     	$data['error'] = $error;
     				
		//css y js de la vista a cargar
          	$data['js_files']=array('subvariable1'=> base_url().'assets/datepicker/js/jquery-1.7.2.min.js','subariable2'=>base_url().'assets/datepicker/js/jquery-ui-1.8.20.custom.min.js','subariable3'=>base_url().'assets/datepicker/js/jquery.ui.datepicker-es.js');
		$data['css_files']=array('subariable2'=>base_url().'assets/datepicker/css/ui-lightness/jquery-ui-1.8.20.custom.css', 'subariable3'=>base_url().'assets/css/rangos_fechas.css');		
		 
		$a= $this->session->userdata('sede_filtro');
		$data['equipos']  = $this->equipos_model->obtener_equipos_sede($this->session->userdata('sede_filtro'));		
		//cargo vista para realizar recepciones
          $data['content_view']='logs/reporte.php';
     	 //template diferente para vistas que no incluyen grocery 
    	 $this->load->view('template2',$data);
      
          }
          else
          {
$desde = $this->general_model->cambia_normal_sql($this->input->post('from'));
$desde  = "'".$desde. " 00:00:00'";
$hasta = $this->general_model->cambia_normal_sql($this->input->post('to'));
$hasta = "'".$hasta. " 23:59:59'";
     	
if ($this->session->userdata('sede_filtro'))
 
 		{$where = "id_sede='".$this->session->userdata('sede_filtro')."' AND fechahora>=".$desde." and fechahora<=".$hasta;}
        //$this->grocery_crud->where('id_sede',$this->session->userdata('sede_filtro'));
        //$this->grocery_crud->where('id_sede',$this->session->userdata('sede_filtro'));
	  	else
      	//$this->grocery_crud->where('id_sede',$sede_consulta);
      	//$this->grocery_crud->where('id_sede',$sede_consulta);
      	{$where = "id_sede='".$sede_consulta."' AND fechahora>=".$desde." and fechahora<=".$hasta;}
        
      $this->grocery_crud->where($where);	



$this->grocery_crud->set_language('spanish');
$this->grocery_crud->columns('tabla','registro','accion','usuario_sistema','fechahora','observaciones');
$this->grocery_crud->unset_read_fields('idregistro','id_sede');
$this->grocery_crud->unset_add();
$this->grocery_crud->unset_edit();
$this->grocery_crud->unset_delete();
$this->grocery_crud->callback_column('accion',array($this,'_callback_columna'));
$this->grocery_crud->callback_column('fechahora',array($this,'_callback_columna1'));
$output = $this->grocery_crud->render();
	$output->content_view='crud_content_view';
$this->_example_output($output);

}
}
}


// Solicito datos para generar el informe 
     	 function generar_reporte($cod = '', $error = ''){
     	 		$this->general_model->validasesion();
     	if	($this->general_model->validapermiso(131072, $this->session->userdata('permisosede')))
 		{
     	     
     	  	 //si hubo cambio de sede actualizo permisos y filtro sede (lo hago aca en las vistas que no incluyen cruds)
      	 	  if(isset($_POST['insumo']))
    	{
        $sede_consulta = $this->input->post('insumo');//sede nueva
        $this->auth_model->cambio_sede($sede_consulta);
    
    	}

    	$data['titulo_reporte'] = "Informe de logs";
     	$data['error'] = $error;
     				
		//css y js de la vista a cargar
          	$data['js_files']=array('subvariable1'=> base_url().'assets/datepicker/js/jquery-1.7.2.min.js','subariable2'=>base_url().'assets/datepicker/js/jquery-ui-1.8.20.custom.min.js','subariable3'=>base_url().'assets/datepicker/js/jquery.ui.datepicker-es.js');
		$data['css_files']=array('subariable2'=>base_url().'assets/datepicker/css/ui-lightness/jquery-ui-1.8.20.custom.css', 'subariable3'=>base_url().'assets/css/rangos_fechas.css');		
		 
		$a= $this->session->userdata('sede_filtro');
		$data['equipos']  = $this->equipos_model->obtener_equipos_sede($this->session->userdata('sede_filtro'));		
		//cargo vista para realizar recepciones
          $data['content_view']='logs/reporte.php';
     	 //template diferente para vistas que no incluyen grocery 
    	 $this->load->view('template2',$data);
      
     	}
     	}





function _example_output($output = null){
// cargo template del sitio y envio la data a traves de output	
$this->load->view('template',$output);
} 

//reemplazo columnas en el listado gral 
public function _callback_columna($value, $row)
{
	if ($value == "I")
	{
return "Alta";		
	}
  elseif ($value == "M")
  {
  	return "Modificaci&oacute;n";
  }
  else
  {
  	return "Baja";
  }
}

public function _callback_columna1($value, $row)
{
	return $value;
	
}



 
 
 
}
    