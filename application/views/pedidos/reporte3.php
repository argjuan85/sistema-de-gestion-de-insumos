




 <script>
  $(function() {
    $( "#from" ).datepicker({
      defaultDate: "",
      changeMonth: true,
      numberOfMonths: 1,
      dateFormat: "dd-mm-yy",
      onClose: function( selectedDate ) {
        $( "#to" ).datepicker( "option", "minDate", selectedDate );
      }
    });
    $( "#to" ).datepicker({
      defaultDate: "",
      changeMonth: true,
      numberOfMonths: 1,
      dateFormat: "dd-mm-yy",
      onClose: function( selectedDate ) {
        $( "#from" ).datepicker( "option", "maxDate", selectedDate );
      }
    });
  });
  </script>




 
<body>
<div id="container">
   <div class="panel-heading titular-fondo-celeste">
<center><h3 class="panel-title"><b><? echo $titulo_reporte; ?></b></h3></center>
 </div><!-- heading -->
      

<p></p>
<?php 
echo form_open('pedidos/reporte3');?>
<div id="d1">
<div id="d1c">

<label for="sectores">Tipo de recepci&oacute;n</label>
<?
echo form_dropdown('tipo_recepcion', $options, 'large');
?>

<span class="text-danger"><?php echo form_error('tipo_recepcion'); ?></span>
</div>
<div id="d2c">
<label for="sectores">N&uacute;mero de recepci&oacute;n</label>
<?	echo form_input('numero', ''); ?>
	<span class="text-danger"><?php echo form_error('numero'); ?></span>	
</div>

 
<div id="d4c">
 <?php echo form_submit('date','Generar');?>
 </div>
  </div>
<?php echo form_close()?>


<p class="footer"></p>
<?php  
if ($error == "vacio")
{
	?>
	<div class="alert alert-danger">
       No se han encontrado pedidos relacionados con la recepci&oacute;n indicada, por favor verifique.
      </div>
	<?
	
	//echo  "No se han encontrado consumos en el rango indicado, por favor verifique.";
echo "<br>";
}
?> 
</div>

</body>
</html>