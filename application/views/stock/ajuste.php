
		<script type="text/javascript" charset="utf-8">
			$(document).ready(function() {
				
				/* fancybox */
				/*$('.fancybox').fancybox();*/
			 $(".fancybox").fancybox({
            type: 'iframe',
             autoSize : false,
             afterClose: function () { // USE THIS IT IS YOUR ANSWER THE KEY WORD IS "afterClose"
                parent.location.reload(true);
            },
              beforeLoad : function() {         
            this.width  = parseInt(this.element.data('fancybox-width'));  
            this.height = parseInt(this.element.data('fancybox-height'));
        },
          helpers : {
        				overlay : {
            			css : {
                		'background' : 'rgba(30, 30, 30, 0.35)'
            				  }
        			}
    }
        
        
        });
				
				/* Init DataTables */
				var oTable = $('#example').dataTable(
				
					{
						"oLanguage": {
							"sProcessing":     "Procesando...",
				    "sLengthMenu":     "Mostrar _MENU_ registros",
				    "sZeroRecords":    "No se encontraron resultados",
				    "sEmptyTable":     "Ningún dato disponible en esta tabla",
				    "sInfo":           "Mostrando registros del _START_ al _END_ de un total de _TOTAL_ registros",
				    "sInfoEmpty":      "Mostrando registros del 0 al 0 de un total de 0 registros",
				    "sInfoFiltered":   "(filtrado de un total de _MAX_ registros)",
				    "sInfoPostFix":    "",
							  "sSearch":         "Buscar:",
							  "sUrl":            "",
				    "sInfoThousands":  ",",
				    "sLoadingRecords": "Cargando...",
				    "oPaginate": {
				        "sFirst":    "Primero",
				        "sLast":     "Último",
				        "sNext":     "Siguiente",
				        "sPrevious": "Anterior"
				    }
				    
						}
					}
		
				
				
				);
				
				/* Apply the jEditable handlers to the table */
				oTable.$('td:eq(1)').editable( '<?php echo site_url('detalle_pedido/editable'); ?>', {
				
					"callback": function( sValue, y ) {
						var aPos = oTable.fnGetPosition( this );
						oTable.fnUpdate( sValue, aPos[0], aPos[1] );
					},
					"submitdata": function ( value, settings ) {
						return {
							"row_id": this.parentNode.getAttribute('id'),
							"column": oTable.fnGetPosition( this )[2]
						};
					},
					"height": "24px",
					"width": "100%",
				
				
				"oLanguage": {
					"sProcessing":     "Procesando...",
				    "sLengthMenu":     "Mostrar _MENU_ registros",
				    "sZeroRecords":    "No se encontraron resultados",
				    "sEmptyTable":     "Ningún dato disponible en esta tabla",
				    "sInfo":           "Mostrando registros del _START_ al _END_ de un total de _TOTAL_ registros",
				    "sInfoEmpty":      "Mostrando registros del 0 al 0 de un total de 0 registros",
				    "sInfoFiltered":   "(filtrado de un total de _MAX_ registros)",
				    "sInfoPostFix":    "",
				    "sSearch":         "Buscar:",
				    "sUrl":            "",
				    "sInfoThousands":  ",",
				    "sLoadingRecords": "Cargando...",
				    "oPaginate": {
				        "sFirst":    "Primero",
				        "sLast":     "Último",
				        "sNext":     "Siguiente",
				        "sPrevious": "Anterior"
				    },
				    "fnInfoCallback": null,
				    "oAria": {
				        "sSortAscending":  ": Activar para ordernar la columna de manera ascendente",
				        "sSortDescending": ": Activar para ordernar la columna de manera descendente"
				    }
					
				}
				
				
				
				} );
				
				
				
			} );
		</script>

<header class="navbar navbar-inverse">
 <div class="container">
  <div class="navbar-collapse nav-collapse collapse navbar-header">
<ul class="nav navbar-nav">

	   		
		   <li class="dropdown">
		    
           <a tabindex="-1" href="<?php echo site_url('stock/listar');?>"><span class="glyphicon glyphicon-chevron-left"> VOLVER </span></a>
          </li>
		   		
   	   
    <li class="dropdown">
		    
           
           <a tabindex="-1" href="<?php echo site_url();?>/parametros/motivos/add" class="fancybox fancybox.iframe" data-fancybox-width="800" data-fancybox-height="370" ><span class="glyphicon glyphicon-plus"> AGREGAR MOTIVO</span></a>
          </li>
          
   
  
  </ul>  <!-- .navbar nav -->  
</div> <!-- .colapse -->  
</div> <!-- .container --> 
 </header> <!-- .navbar -->	


 
<body>
<div id="container">
 
      

<p></p>
<?php 
$cod = $this->uri->segment(3);
echo form_open('stock/ajuste2/'.$cod);?>
<div id="d1">
<div id="d1c">
	
<label for="sectores">Motivo</label>

<? echo form_dropdown('motivos', $motivos);?>
<span class="text-danger"><?php echo form_error('motivos'); ?></span>



</div>
<div id="d2c">
<label for="sectores">Nueva Cantidad</label>
<?	echo form_input('cantidad', ''); ?>
	<span class="text-danger"><?php echo form_error('cantidad'); ?></span>	
</div>

 
<div id="d4c">
 <?php echo form_submit('date','Generar','class="edit_button ui-button ui-widget ui-state-default ui-corner-all ui-button-text-icon-primary", onclick="if(!confirm(&quot; Es correcta la cantidad a cargar?&quot;))return false;" ');?>
 </div>
  </div>
<?php echo form_close()?>


<p class="footer"></p>
<?php  
if ($error == "vacio")
{
	?>
	<div class="alert alert-danger">
       Debe ingresar una cantidad distinta a la actual.
      </div>
	<?
	
	
	//echo  "No se han encontrado consumos en el rango indicado, por favor verifique.";
echo "<br>";
}
?> 
</div>

</body>
</html>