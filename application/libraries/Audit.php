<?php

/**
 * Provee la implementacion para el registro de auditorias del sistema.
 *
 * @author  Jose Rodriguez <josearodrigueze@gmail.com> @josearodrigueze
 * @link    http://josearodrigueze.wordpress.com/
 * @version 1.0 21/04/2013
 */
class Audit {

    /**
     * Contiene la instancia de CI
     * @var $CI CodeIgniter
     */
    private $CI = NULL;

    /**
     * Contiene el resultado de la operacion ([Default] ACCESS, SUCCESS, FAILURE).
     * @var $result String
     */
    private $result;

    /**
     * Contiene los datos utlizados en la operaion.
     * @var $result Mixed
     */
    private $data;

    public function __construct() {
        $this->CI = &get_instance();
        $this->result = 'ACCESS';
        $this->data = NULL;
        $this->CI->load->library('user_agent');
        $this->CI->load->helper('url');
        $this->CI->load->model('audit_model');
    }

    public function getResult() {
        return $this->result;
    }

    public function setResult($result) {
        if($result === TRUE)
            $this->result = 'SUCCESS';
        elseif($result === FALSE)
            $this->result = 'FAILURE';
    }

    public function getData() {
        return $this->data;
    }

    public function setData($data) {
        $this->data = $data;
    }

    /**
     * Realiza el registro de auditorias del sistema.
     *
     * @author  Jose Rodriguez <josearodrigueze@gmail.com> @josearodrigueze
     * @link    http://josearodrigueze.wordpress.com/
     * @version 1.0 21/04/2013
     */
    function register() {
        $data = array(
            'user_id' => $this->CI->session->userdata('user_id'),
            'url' => base_url($this->CI->input->server('REQUEST_URI')),
            'moment' => date('d-m-Y H:i:s'),
            'ip' => $this->CI->input->ip_address(),
            'browser' => $this->CI->agent->browser(),
            'version' => $this->CI->agent->version(),
            'os' => $this->CI->agent->platform(),
            'result' => $this->getResult()
        );
        if (!empty($this->data))
            $data['data'] = json_encode ($this->getData());
        
//        echo '<pre>', print_r($data, TRUE), '</pre>';die;
        
        $this->CI->audit_model->add($data);
    }

}