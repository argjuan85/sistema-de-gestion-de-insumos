<?php
class General_model extends CI_Model
	{
		
		public function __construct()
	{
		parent::__construct();
	}
	
	
//funcion para validar la inactividad de un user logueado
	public function valida_tiempo_sesion ()
	{ 
			$caduca = $this->parametros_model->obtener_parametro($this->parametros_model->obtener_id_parametro_nombre("0","caduca_sesion"));
			$fechaOld= $_SESSION["ultimoAcceso"];
			$ahora = date("Y-n-j H:i:s");
			$a = strtotime($ahora);
			$b = strtotime($fechaOld);
			$tiempo_transcurrido = (strtotime($ahora)-strtotime($fechaOld));
			if($tiempo_transcurrido>= $caduca) //me fijo si no caduco la sesion $caduca es un parametro
				{ 	
				
					$this->session->set_flashdata('msg', '<div class="alert alert-danger text-center">Su sesión caducó por inactividad. Por favor, ingrese nuevamente.</div>');
                         //redirect('auth/inicio');
                         redirect(site_url("auth/inicio"),"refresh");
					return false;
				}
		else
		{
			$_SESSION["ultimoAcceso"] = date("Y-n-j H:i:s");
			return true;
		}
	}
		// valido si el usuer logueado tiene permiso para acceder a la pagina, recibo el id de la pagina y lo comparo con el permiso del user
	public function validapermiso ( $nivelpagina , $permiso_user  )
	{ 
		$this->valida_tiempo_sesion ();
		IF ( ($nivelpagina & $_SESSION['permisosede']) == $nivelpagina )
		{
		return true;
		}
		else 
		{	
			$this->session->set_flashdata('msg', '<div class="alert alert-danger text-center">No tiene permisos para acceder a esta página</div>');
			//redirect('auth/permisos');
			redirect(site_url("auth/permisos"),"refresh");
		return false;
		}
	
	}
	
	
			// valido si el usuer logueado tiene permiso para acceder a la pagina, recibo el id de la pagina y lo comparo con el permiso del user
	public function validasede ( $sede_registro  )
	{ 
		
	if ($sede_registro == $this->session->userdata('sede_filtro'))
	{
		return TRUE;
	}
	else
	{
		//redirect('auth/acceso');
		redirect(site_url("auth/acceso"),"refresh");
	}
	
	}
	
		public function validasede1 ( $sede_registro, $proovedor_registro  )
	{ 
		
	if (  ($sede_registro == $this->session->userdata('sede_filtro')) || ($proovedor_registro == $this->session->userdata('sede_filtro'))  )
	{
		return TRUE;
	}
	else
	{
		redirect(site_url("auth/acceso"),"refresh");
	}
	
	}
	
	
		//para mostrar los tipos de pedido
		public function label_pedido($tipo)
		{  
		if ( ($tipo == "I") ||  ($tipo == "P") )
		return "Interno";
		else 
		return "Externo";
		}
    //////////////////////////////////////////////////// 
//Convierte fecha de 2010-10-08 a 08-10-2010
//////////////////////////////////////////////////// 

function cambia_sql_normal($fecha){ 
    preg_match( "/([0-9]{2,4})-([0-9]{1,2})-([0-9]{1,2})/", $fecha, $mifecha); 
    $lafecha=$mifecha[3]."-".$mifecha[2]."-".$mifecha[1]; 
    return $lafecha; 
}


    //////////////////////////////////////////////////// 
//Convierte fecha de 08-10-2010 a 2010-10-08
//////////////////////////////////////////////////// 

function cambia_normal_sql($fecha){ 
    preg_match( "/([0-9]{2,2})-([0-9]{1,2})-([0-9]{1,4})/", $fecha, $mifecha); 
    $lafecha=$mifecha[3]."-".$mifecha[2]."-".$mifecha[1]; 
    return $lafecha; 
}

function ou_sede_id ($ou)
{
		switch ($ou)
	{
		
		
		case "OU SJ":{
			$id = $this->sedes_model->obtener_id_sede("San Juan");
			break;
			}
		case "OU BA":{
			$id = $this->sedes_model->obtener_id_sede("Buenos Aires");
			break;
			}
			case "OU MUNRO":{
			$id = $this->sedes_model->obtener_id_sede("Munro");
			break;
			}		
			
	}
	
	 return $id;
}

function registralog ( $accion , $tabla , $idregistro , $registro, $observaciones = NULL )
	{ 
		       
            $equipo = gethostbyaddr($_SERVER['REMOTE_ADDR']);
            $user = $_SESSION["username"];
            $sede = $_SESSION["sede_filtro"];
            $ip = $this->input->ip_address();
            $browser = $this->agent->browser();
            $version = $this->agent->version();
            $os = $this->agent->platform();
            
		 

	if ( $observaciones != NULL)
	{
		
			$sql = "INSERT INTO logs (tabla, idregistro, registro, accion, equipo, usuario_sistema, observaciones,ip, browser, version, os, id_sede) 
		     VALUES ( '$tabla', '$idregistro', '$registro', '$accion', '$equipo', '$user', '$observaciones','$ip', '$browser', '$version', '$os', '$sede')";
    }
    else
    {
    			$sql = "INSERT INTO logs (tabla, idregistro, registro, accion, equipo, usuario_sistema, ip, browser, version, os, id_sede) 
		     VALUES ( '$tabla', '$idregistro', '$registro', '$accion', '$equipo', '$user', '$ip', '$browser', '$version', '$os', '$sede')";
		
	}
    		  
		     $consulta=$this->db->query($sql);
		     return;
		
	}
	
	function validasesion () 
{

if ( ( isset($_SESSION["loginuser"])) && (isset($_SESSION["nivel_sede"]))  && (isset($_SESSION["permisosede"])) )
	{if ($_SESSION["loginuser"] == TRUE)
		{return true;}
		else
		{
		$this->session->set_flashdata('msg', '<div class="alert alert-danger text-center">No tiene permisos para acceder a esta página</div>');
			//redirect('auth/inicio');
			redirect(site_url("auth/inicio"),"refresh");
		return false;	
		
		}
	}
else
{
		$this->session->set_flashdata('msg', '<div class="alert alert-danger text-center">No tiene permisos para acceder a esta página</div>');
			//redirect('auth/inicio');
			redirect(site_url("auth/inicio"),"refresh");
		return false;
}
	
	
				
				
}
	


}
?>