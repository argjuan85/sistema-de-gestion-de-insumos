<?php

//seria importante chequear que las consultas esten ok en alguna parte para evitar errores no amigables en el sitio
	class Parametros_model extends CI_Model
	{
		
			
		//dado un nombre  y valor  de param devuelve el id
		public function obtener_id_parametro($nombre,$valor)
		{   
			$this->load->database();
		$sql= 	'select id from Parametros where nombre_parametro= '.'"'.$nombre.'"'.' and valor= '.'"'.$valor.'"';
		$query = $this->db->query($sql);
			
			$nombre=$query->row();
			return $nombre->id;
			
		}



		
				//dado un nombre  de param devuelve el id
		public function obtener_id_parametro_nombre($sede,$nombre)
		{   
			$this->load->database();
			
		$query = $this->db->query('select id from Parametros where nombre_parametro= '.'"'.$nombre.'"'.' and id_sede= '.'"'.$sede.'"');
			
			$nombre=$query->row();
			return $nombre->id;
			
		}
		
			//dado un id de param devuelve el valor
		public function obtener_parametro($id)
		{   
			$this->load->database();
			
		$query = $this->db->query('select valor from Parametros where id= '.'"'.$id.'"');
			
			$nombre=$query->row();
			return $nombre->valor;
			
		}
		
					//dado un id de param devuelve el valor
		public function verificar_habilitado($id)
		{   
			$this->load->database();
			
		$query = $this->db->query('select habilitado from Parametros where id= '.'"'.$id.'"');
			
			$nombre=$query->row();
			return $nombre->habilitado;
			
		}
		
		
		//modifica valor parametro dado un id
				
		public function modifica_parametro($id,$valor)
		{   
			$this->load->database();
			$sql = "update Parametros set valor= ". "'" .$valor."' ";
			$sql = $sql . " where id= " ."'".$id."'";
			$query = $this->db->query($sql);
			
			/*$nombre=$query->row();
			return $nombre->valor;*/
			
		}
		//verifica que ante la carga de un parametro no exista uno similar
			public function verificar_param_repetido($nombre_param , $valor)
		{  
			$this->load->database();
			$sql = 'select id, nombre_parametro from Parametros where nombre_parametro= '.'"'.$nombre_param.'"'.' and valor= '.'"'.$valor.'"';
			$consulta=$this->db->query($sql);
			if ($consulta->num_rows() > 0) {
				return FALSE; // ya existe un param cargado		
				}
			 else 
			 return TRUE; // no existe
		}
		
				//verifica que ante la carga de un parametro no este vacio
			public function verificar_param_vacio($valor)
		{  
				if ($valor == "") {
				return TRUE; // vacio
				}
			 else 
			 return False; 
		}
		
		
			//verifica que ante la carga de un registro no exista uno similar salvo correccion
			public function verificar_registro_repetido($registro , $nombre_campo, $tabla, $clave, $id_edit= "-1", $id_sede= "-1")
		{  
			$this->load->database();
			if ($id_sede != "-1")
			{
			$sql = 'select '.$clave.' from '.$tabla.' where '.$nombre_campo.'= '.'"'.$registro.'" and id_sede='.'"'.$id_sede.'"';	
			}
			else
			{
						
			$sql = 'select '.$clave.' from '.$tabla.' where '.$nombre_campo.'= '.'"'.$registro.'"';
			}
			$consulta=$this->db->query($sql);
			if ($consulta->num_rows() > 0)
			 {
			 	$consulta=$consulta->row();
				$id = $consulta->id;
				if ($consulta->id == $id_edit)
				{
					return TRUE; // es el mismo registro		
				}
				else
				{
					Return false; //es otro registro
				}
			}
			 else 
			 {
			 return TRUE; // no existe
			 }
		}
		
		
		
		//obtiene los grupos de ad para el sistema
			public function obtener_grupos()
		{   
			$this->load->database();
			$sql = 'select nombre_parametro, valor from Parametros where nombre_parametro="grupo_ad"';
			$consulta=$this->db->query($sql);
			 if ($consulta->num_rows() > 0) {
			$nombresx=$consulta->result_array();
			}
			else
			{$nombresx = "-1";}
			return $nombresx;
		}
		
		//dado el identificador de la sede en un grupo devuelve el nombre completp
				public function sigla_sede($sede)
		{   
			switch ($sede)
			{
				case "APSJ":{
					$nombresx= "San Juan";
					break;
				}
				case "APVM":{
					$nombresx= "Villa Martelli ";
					break;
				}
				case "APTO":{
					$nombresx= "Tortuguitas";
					break;
				}
				case "APMU":{
					$nombresx= "Munro";
					break;
				}
			}
			return $nombresx;
		}
		
			//dado el OU de la sede en un grupo devuelve el nombre completp
				public function ou_sede($sede)
		{   
			switch ($sede)
			{
				case "OU SJ":{
					$nombresx= "San Juan";
					break;
				}
					case "OU VM":{
					$nombresx= "Villa Martelli";
					break;
				}
				case "OU PMTVBA":{
					$nombresx= "Tortuguitas";
					break;
				}
				case "OU MUNRO":{
					$nombresx= "Munro";
					break;
				}
			}
			return $nombresx;
		}
		
		//dado el nombre de la sede en un grupo devuelve el nombre del identif
				public function sede_sigla($sede)
		{   
			switch ($sede)
			{
				case "San Juan":{
					$nombresx= "APSJ";
					break;
				}
				case "Munro":{
					$nombresx= "APMU";
					break;
				}
				case "Villa Martelli":{
					$nombresx= "APVM";
					break;
				}
				case "Tortuguitas":{
					$nombresx= "APTO";
					break;
				}
			}
			return $nombresx;
		}
		
		//obtiene los grupos de ad para una sede 
			public function obtener_grupos_sede($sede)
		{   
			$grupos = $this->obtener_grupos();//ok
			foreach ($grupos as $grupo)
			{
				//$borrar = $this->auth_model->obtener_sede_grupo($grupo['valor']);
				if ($this->auth_model->obtener_sede_grupo($grupo['valor']) == $sede)	
				//if ( "3" == $sede)
				{
					$xgrupo[] = $grupo['valor'];//armo array
				}
				
			}
			
			return $xgrupo;
		}
		
			//obtiene los motivos para ajuste de stock
			public function obtener_motivos()
		{   
			$this->load->database();
			$sql = 'select id, valor from parametros where nombre_parametro= "motivo_ajuste" and habilitado="1"';
			$query=$this->db->query($sql);
			$arrDatos['-1'] = 'Seleccione una opción';//asigno -1 por que en la validacion pregunto por id positivos en el dropdown
		        
			 if ($query->num_rows() > 0) {
        // almacenamos en una matriz bidimensional
        foreach($query->result() as $row)
           $arrDatos[htmlspecialchars($row->id, ENT_QUOTES)] = 
htmlspecialchars($row->valor, ENT_QUOTES);

        $query->free_result();
        
		}
		return $arrDatos;
		}
		
		//obtiene los tipos de recepcion parametrizados
			public function obtener_tipos_recepcion()
		{   
			$this->load->database();
			$sql = 'select id, valor from parametros where nombre_parametro= "tipo_recepcion" and habilitado="1"';
			$query=$this->db->query($sql);
			$arrDatos['-1'] = 'Seleccione una opción';//asigno -1 por que en la validacion pregunto por id positivos en el dropdown
		        
			 if ($query->num_rows() > 0) {
        // almacenamos en una matriz bidimensional
        foreach($query->result() as $row)
           $arrDatos[htmlspecialchars($row->id, ENT_QUOTES)] = 
htmlspecialchars($row->valor, ENT_QUOTES);

        $query->free_result();
        
		}
		return $arrDatos;
		}
		
		
					//dado un nombre  de grupo de ad devuelve el nivel de permiso asociado
		/*public function obtener_nivel_grupo($grupo)
		{   
			$this->load->database();
			$sql = 'select valor from Parametros where nombre_parametro= '.'"'.$grupo.'"';
			$query = $this->db->query($sql);
			$nombre=$query->row();
			return $nombre->valor;
			
		}*/
		
		
	}
?>