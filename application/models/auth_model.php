<?php
class auth_model extends CI_Model
	{
		
		public function __construct()
	{
		parent::__construct();
	}
	
	//un user con permisos en mas de una sede al cambiar en el combo debe actualizar su permiso, recibo la sede nueva, y devuelvo el permiso del grupo en donde pertenece dicho user.
	public function cambio_sede($sede)
	{
		
	$sede_consulta = $sede;//sede nueva
        $this->session->set_userdata('sede_filtro', $sede_consulta );
        
        $usuario = $this->session->userdata('username');
		//busco los permisos para la sede elegida, hardcodeo para test
		$permisos_sede = $this->auth_model->obtener_permiso_sede($sede_consulta, "-1", $usuario);
		 $this->session->set_userdata('permisosede', $permisos_sede );
	}
	
	//los usuarios con acceso a mas de una sede. deben cargar los permisos al cambiar de sede
	public function obtener_permiso_sede($sede, $conectado_LDAP, $usuario)
	{
		//obtengo los grupos de la Sedes
		$grupos = $this->parametros_model->obtener_grupos_sede($sede);//hasta aca ok
		
	//obtengo el permiso del user para el grupo, aca harcodeo
		$permiso = $this->obtener_permiso($grupos, $usuario, $conectado_LDAP);
		return $permiso;
	}

		/*
* This function searchs in LDAP tree ($ad -LDAP link identifier)
* entry specified by samaccountname and returns its DN or epmty
* string on failure.
*/
public function getDN($ad, $samaccountname, $basedn) {
    $attributes = array('dn');
    $result = ldap_search($ad, $basedn,
        "(samaccountname={$samaccountname})", $attributes);
    if ($result === FALSE) { return ''; }
    $entries = ldap_get_entries($ad, $result);
    if ($entries['count']>0) { return $entries[0]['dn']; }
    else { return ''; };
}
	//paso array de grupos de sede y un usuario, luego veo en cual tiene permisos y en base a eso asigno el nivel correspondient
	public function obtener_permiso($grupos, $usuario_LDAP, $conexion = '-1', $cambio = '-1')
	{
		$tienepermiso= "0";// esta bandera indica si el user pertenece al menos a 1 grupo (para loguear al sistema)
		$data['nivel']= "0"; //calcula nivel de permisos por sede (Se maneja por pesos establecidos en la tabla parametros)
		$unicasede = "0"; // uso esta var para saber si teng permisos en mas de una sede y mostrar un select de sedes en caso afirmativo
	
  //desactivamos los erroes por seguridad
  error_reporting(0);
  //error_reporting(E_ALL); //activar los errores (en modo depuración)

if ($conexion == "-1")
{
	$cambio = '1';	
//cargo parametros con la info para conectar al AD
  $servidor_LDAP = $this->parametros_model->obtener_parametro($this->parametros_model->obtener_id_parametro_nombre("0","servidor_ad"));
   $servidor_dominio = $this->parametros_model->obtener_parametro($this->parametros_model->obtener_id_parametro_nombre("0","Dominio"));
   $ldap_dn = $this->parametros_model->obtener_parametro($this->parametros_model->obtener_id_parametro_nombre("0","dn_ad"));
  //$grupos = $this->parametros_model->obtener_grupos_sede();
  

  //echo "<h3>Validar en servidor LDAP desde PHP</h3>";
  //echo "Conectando con servidor LDAP desde PHP...";

  $conectado_LDAP = ldap_connect($servidor_LDAP);//conexion 
  $contrasena_LDAP = $this->session->userdata('key');
  ldap_set_option($conectado_LDAP, LDAP_OPT_PROTOCOL_VERSION, 3);
  ldap_set_option($conectado_LDAP, LDAP_OPT_REFERRALS, 0);
}
else
{
	$conectado_LDAP = $conexion;
}
  if ($conectado_LDAP) 
  {
       if ($conexion == "-1")
		{
	   $autenticado_LDAP = ldap_bind($conectado_LDAP, $usuario_LDAP . "@" . $servidor_dominio, $contrasena_LDAP);
    	}
    	if ( ($conexion != "-1") || ($autenticado_LDAP))
	    {
			
		
	   
	    // echo "<br>Autenticación en servidor LDAP desde Apache y PHP correcta.";
		$ldap_dn = $this->parametros_model->obtener_parametro($this->parametros_model->obtener_id_parametro_nombre("0","dn_ad"));
	    $userdn = $this->getDN($conectado_LDAP, $usuario_LDAP, $ldap_dn);
			
	
	    //$sss = "2k";
	    //$userdn = $this->getDN($conectado_LDAP, $usuario_LDAP, $ldap_dn);
 		//$grup_dn = $this->getDN($conectado_LDAP, $grupo['valor'], $ldap_dn);
	    //recorro los grupos definidos en los parametros para buscar la membresia del usuario autenticado y calcular permiso sobre sedes
	    foreach($grupos as $grupo)
	  {
	  	$gr = $grupo;
	  	$grupdn = $this->getDN($conectado_LDAP, $gr, $ldap_dn);
        $pertenece = $this->checkGroup($conectado_LDAP, $userdn, $grupdn);
       
         //$pertenece2 = $this->checkUserInGroups($userdn, $grupo['valor'], $this->getDN($conectado_LDAP, $grupo['valor'], $ldap_dn));
      
         //al no haber grupos definidos no hay membresia simulo que pertenece a un grupo en particular ( en este caso el primero del array)
       
         
         //fin harcode despues descomentar la linea de arriba
         If ($pertenece == True)
         {
         	$permisosede = $this->obtener_permiso_grupo($grupo);//aqui asigno el permiso asociado al grupo 
         	break;
		 } 
		 else 
		 {
		 	$permisosede = "-1";
		 }
      }
      //solo cierro la conexoin al AD cuando es un cambio de sede por menu,  si no es por que me estoy logueando y debo mantener la conexion para futuras consultas en la funciones de logueo
      if ($cambio == "1")
      {
      	
      	ldap_unbind($conectado_LDAP);
      
      	}//cierro conexion AD
      	
     }
  }
  else 
  {
    /*echo "<br><br>No se ha podido realizar la conexión con el servidor LDAP: " .
        $servidor_LDAP;*/
  }
  
return $permisosede;
	}
	
			//dado el nombre del grupo devuelvo el nivel de permisos asociado
		public function obtener_permiso_grupo($grupo)
		{   
			$this->load->database();
		$sql= 	'select valor from Parametros where nombre_parametro= '.'"'.$grupo.'"';
		$query = $this->db->query($sql);
			$nombre=$query->row();
			return $nombre->valor;
			
		}
		
		//si hay que agregar una sede, ademas de cargar en el abm se debe agregar en esta funcion.
public function obtener_sede_grupo($dn) {
	
	//el criterio de busqueda dependera del formato de los grupos designados en el AD
	$findme = $rest = substr($dn, 0, 4);
	switch ($findme)
	{
		//definir demas sedes cuando ya esten los grupos
		
		case "APSJ":{
			$id = $this->sedes_model->obtener_id_sede("San Juan");
			break;
			}
		case "APVM":{
			$id = $this->sedes_model->obtener_id_sede("Villa Martelli");
			break;
			}	
		case "APMU":{
			$id = $this->sedes_model->obtener_id_sede("Munro");
			break;
			}	
		case "APTO":{
			$id = $this->sedes_model->obtener_id_sede("Tortuguitas");
			break;
			}
			
	}
	
	 return $id;
   
}

 function checkUserInGroups($user, $groupsToFind, $grup_dn, $ldapconn) {
  
     
        /* Recorremos el array donde se almacenan los grupos con permisos de lectura */
       // for($group=0;$group<count($groupsToFind);$group++){
            /* $groupDN = "CN=".$groupsToFind[$group].
                        ",OU=Usuarios,OU=Grupos,OU=miEmpresa,DC=raffo,DC=local";*/
             $groupDN = $grup_dn;
             $filter = '(memberof:1.2.840.113556.1.4.1941:='.$groupDN.')';
             /*$userDN = _LDAP_DN_;
             $userDN = str_replace('*replace_name*', $user, $userDN);*/
             $userDN = $user;
             $search = ldap_search($ldapconn, $userDN, $filter, array('dn'), 1);
             $items = ldap_get_entries($ldapconn, $search);
             if ($items['count'] > 0){
                //ldap_close($ldapconn);
                return true;
            }
       // }
 
        /* Si llegamos a este punto, el usuario NO se encuentra en el grupo  */
     
        return false;
 
   
   
}

/*
* This function checks group membership of the user, searching only
* in specified group (not recursively).
*/
public function checkGroup($ad, $userdn, $groupdn) {
    $attributes = array('members');
    $result = ldap_read($ad, $userdn, "(memberof={$groupdn})", $attributes);
    if ($result === FALSE) { return FALSE; };
    $entries = ldap_get_entries($ad, $result);
    return ($entries['count'] > 0);
}

/*
* This function checks group membership of the user, searching
* in specified group and groups which is its members (recursively).
*/
public function checkGroupEx($ad, $userdn, $groupdn) {
    $attributes = array('memberof');
    $result = ldap_read($ad, $userdn, '(objectclass=*)', $attributes);
    if ($result === FALSE) { return FALSE; };
    $entries = ldap_get_entries($ad, $result);
    if ($entries['count'] <= 0) { return FALSE; };
    if (empty($entries[0]['memberof'])) { return FALSE; } else {
        for ($i = 0; $i < $entries[0]['memberof']['count']; $i++) {
            if ($entries[0]['memberof'][$i] == $groupdn) { return TRUE; }
            elseif ($this->checkGroupEx($ad, $entries[0]['memberof'][$i], $groupdn)) { return TRUE; };
        };
    };
    return FALSE;
}	

}
?>