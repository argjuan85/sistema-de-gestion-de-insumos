<?php

/**
 * Clase que realiza las operaciones en BD de auditoria del sistema.
 *
 * @author  Jose Rodriguez <josearodrigueze@gmail.com> @josearodrigueze
 * @link    http://josearodrigueze.wordpress.com/
 * @version 1.0 21/04/2013
 */
class Audit_model extends CI_Model {

    /** 
     * Contiene el nombre de la tabla donde se almacena los datos.
     * @var $table string
     */
    private $table = NULL;

    public function __construct() {
        parent::__construct();
        $this->table = 'audit';
    }

    /**
     * Metodo que almacena en BD los datos contenidos en $param.
     *
     * @param   array   $param
     * @author  Jose Rodriguez <josearodrigueze@gmail.com> @josearodrigueze
     * @link    http://josearodrigueze.wordpress.com/
     * @version 1.0 21/04/2013
     */
    function add($param) {
        $this->db->insert($this->table, $param);
        return ($this->db->affected_rows()) ? TRUE : FALSE;
    }

}